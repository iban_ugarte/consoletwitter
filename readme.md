# ConsoleTwitter

### How to execute it

##### From your IDE (eclipse, Intellij..)

- Open as Maven project, compile and run it

##### From comand line: 

Navigate to main folder: `~/ConsoleTwitter`

- ###### Executing using jar:

  - Execute `$ mvn package`
  - Go to `target` folder
  - Exexute `$ java -cp ConsoleTwitter-1.0-SNAPSHOT.jar org.exercise.ctt.App`

- ###### Executing using maven:

  - `$ mvn exec:java -Dexec.mainclass="org.exercise.ctt.App"`

To stop executing press `Ctrl +C`

### Play with the ConsoleTwitter

You can't create users. You have to use the predefined ones: **bob, alice, charlie, john & sarah** 

 - Post:   [username] -> message  `$ bob -> this is my post`
 - Read:   [username] `$ bob` 
 - Follow: [username] follows [username] `$ bob follows alice`
 - Wall:   [username] wall `$bob wall`