package org.exercise.ctt;

import org.exercise.ctt.repository.local.LocalRepository;
import org.exercise.ctt.service.ConsoleTwitterManager;

public class App
{
    public static void main( String[] args )
    {
        ConsoleTwitterManager
                .withRepo(new LocalRepository())
                .run();

    }

}
