package org.exercise.ctt.adapter.input;

import org.exercise.ctt.model.User;

public class FollowingInput implements IInput{

    private final User user;
    private final User following;

    public FollowingInput(User user, User following) {
        this.user = user;
        this.following = following;
    }

    public User getUser() {
        return user;
    }

    public User getFollowing() {
        return following;
    }
}
