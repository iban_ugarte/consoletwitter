package org.exercise.ctt.adapter.input;

import org.exercise.ctt.adapter.input.util.AdapterUtil;
import org.exercise.ctt.command.ConsoleCommand;
import org.exercise.ctt.exception.ConsoleTwitterException;
import org.exercise.ctt.model.User;

import java.util.Arrays;
import java.util.List;

public enum InputHandler {

    INSTANCE;

    public ConsoleCommand getConsoleCommand(String consoleInput) throws ConsoleTwitterException {
        String[] inputs = getSplittedInput(consoleInput);
        if(inputs.length == 1){
            return ConsoleCommand.READ;
        }

        return ConsoleCommand.getConsoleCommand(inputs[1])
                .orElseThrow(ConsoleTwitterException::invalidCommand);

    }

    public IInput getInput(ConsoleCommand consoleCommand, String consoleInput) throws ConsoleTwitterException{
        String[] inputs = getSplittedInput(consoleInput);
        switch (consoleCommand){
            case POST:
                return getPostingInput(inputs);
            case FOLLOW:
                return getFollowingInput(inputs);
            case READ:
                return getReadingInput(inputs);
            case WALL:
                return getWallInput(inputs);
            default:

        }
        throw new IllegalArgumentException(String.format("El comando %s no es válido", consoleCommand.name()));
    }

    private PostingInput getPostingInput(String[] inputs) throws ConsoleTwitterException {

        List<String> inputList = Arrays.asList(inputs);
        if(isNotValidThirdParameter(inputList)){
            throw ConsoleTwitterException.requiredMessage();
        }

        String message = getMessage(inputList);
        User user = new User(inputList.get(0));
        return new PostingInput(user, message);
    }


    private FollowingInput getFollowingInput(String[] inputs) throws ConsoleTwitterException {

        List<String> inputList = Arrays.asList(inputs);
        if(isNotValidThirdParameter(inputList)){
            throw ConsoleTwitterException.requiredUserToFollow();
        }

        User user = new User(inputs[0]);
        String follow = getMessage(inputList);
        User followingUser = new User(follow);
        return new FollowingInput(user, followingUser);

    }

    private ReadingInput getReadingInput(String[] inputs) {
        User user = new User(inputs[0]);
        return new ReadingInput(user);
    }

    private WallInput getWallInput(String[] inputs) {
        User user = new User(inputs[0]);
        return new WallInput(user);
    }


    private static String[] getSplittedInput(String input) throws ConsoleTwitterException {
        return AdapterUtil.getSplittedInput(input);
    }

    private String getMessage(List<String> inputList) {
        return AdapterUtil.getMessage(inputList);
    }

    private boolean isNotValidThirdParameter(List<String> inputList) {
        return AdapterUtil.isNotValidThirdParameter(inputList);
    }

}
