package org.exercise.ctt.adapter.input;

import org.exercise.ctt.model.Post;
import org.exercise.ctt.model.User;

public class PostingInput implements IInput{

    final User user;
    final Post post;

    public PostingInput(User user, String message){
        this.user = user;
        this.post = Post.of(user, message);
    }

    public User getUser() {
        return user;
    }

    public Post getPost() {
        return post;
    }
}
