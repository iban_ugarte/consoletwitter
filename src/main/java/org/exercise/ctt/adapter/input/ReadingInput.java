package org.exercise.ctt.adapter.input;

import org.exercise.ctt.model.User;

public class ReadingInput implements IInput{

    private final User user;

    public ReadingInput(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
