package org.exercise.ctt.adapter.input;

import org.exercise.ctt.model.User;

public class WallInput implements IInput{

    private final User user;

    public WallInput(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
