package org.exercise.ctt.adapter.input.util;

import org.exercise.ctt.exception.ConsoleTwitterException;

import java.util.List;

public class AdapterUtil {

    private AdapterUtil(){}

    public static String[] getSplittedInput(String input) throws ConsoleTwitterException {
        try {
            return input.split("\\s+");
        }catch (Exception e){
            throw ConsoleTwitterException.unexpected(e);
        }
    }

    public static String getMessage(List<String> inputList) {

        StringBuilder message = new StringBuilder();
        for(int i = 2; i < inputList.size(); i++){
            message.append(inputList.get(i)).append(" ");
        }
        return message.toString().trim();
    }

    public static boolean isNotValidThirdParameter(List<String> inputList) {
        return inputList.size() < 3;
    }
}
