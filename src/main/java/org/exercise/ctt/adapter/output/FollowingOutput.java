package org.exercise.ctt.adapter.output;

import org.exercise.ctt.model.User;

public class FollowingOutput implements IOutput{

    private final String FORMAT = "\t%s is now following >>> %s";
    private final User user;
    private final User following;

    private FollowingOutput(User user, User following){
        this.user = user;
        this.following = following;
    }

    public static FollowingOutput following(User user, User follower){
        return new FollowingOutput(user, follower);
    }

    @Override
    public void out() {
        System.out.printf(FORMAT,user.getName(), following.getName());
    }
}
