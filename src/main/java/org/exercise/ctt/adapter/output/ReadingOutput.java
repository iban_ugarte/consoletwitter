package org.exercise.ctt.adapter.output;

import org.exercise.ctt.model.Post;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class ReadingOutput implements IOutput{

    private final String FORMAT = "\t · %s\n";
    private final List<Post> posts;

    private ReadingOutput(List<Post> posts){
        this.posts = posts;
    }

    public static ReadingOutput of(List<Post> posts){
        if(Objects.isNull(posts)){
            return emptyOutput();
        }
        return new ReadingOutput(posts);

    }

    public static ReadingOutput emptyOutput(){
        return new ReadingOutput(Collections.emptyList());
    }

    public List<Post> getPosts() {
        return Collections.unmodifiableList(posts);
    }

    @Override
    public void out() {
        this.posts.forEach(p -> System.out.printf(FORMAT, p.print()));
    }
}
