package org.exercise.ctt.adapter.output;

import org.exercise.ctt.model.Post;
import org.exercise.ctt.model.User;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class WallOutput implements IOutput{

    public static final String FORMAT = "[%s] %s";
    private final List<Post> wall;
    private final User user;

    public WallOutput(User user, List<Post> posts){
        this.user = user;
        if(Objects.isNull(posts)){
            this.wall = Collections.emptyList();
        } else {
            this.wall = posts;
        }
    }

    @Override
    public void out() {
        System.out.println("The wall of " + user.getName());
        wall.stream()
                .sorted(Comparator.comparing(Post::getDateTime))
                .forEach(p -> System.out.println(
                        String.format(FORMAT, p.getUser().getName(), p.print())));
    }
}
