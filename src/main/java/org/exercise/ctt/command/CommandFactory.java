package org.exercise.ctt.command;

import org.exercise.ctt.repository.ConsoleTwitterRepository;

public class CommandFactory {

    private final ConsoleTwitterRepository repository;

    private CommandFactory(ConsoleTwitterRepository repository){
        this.repository = repository;
    }

    public static CommandFactory withRepo(ConsoleTwitterRepository repository){
        return new CommandFactory(repository);
    }

    public ICommand commandOf(ConsoleCommand command){
        switch (command){
            case POST:
                return new PostingCommand(repository);
            case FOLLOW:
                return new FollowingCommand(repository);
            case READ:
                return new ReadingCommand(repository);
            case WALL:
                return new WallCommand(repository);
            default:
        }
        throw new IllegalArgumentException(String.format("El comando %s no es válido", command.name()));
    }

}
