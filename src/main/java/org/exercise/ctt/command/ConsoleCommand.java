package org.exercise.ctt.command;

import java.util.Arrays;
import java.util.Optional;

public enum ConsoleCommand {
    POST("->"),
    READ("timeline"),
    FOLLOW("follows"),
    WALL("wall");

    private final String value;

    ConsoleCommand(String value){
        this.value = value;
    }

    public static Optional<ConsoleCommand> getConsoleCommand(String value){
        return Arrays.stream(ConsoleCommand.values())
                .filter(c -> value.equalsIgnoreCase(c.value))
                .findFirst();
    }
}
