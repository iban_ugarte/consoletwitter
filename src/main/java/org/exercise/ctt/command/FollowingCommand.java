package org.exercise.ctt.command;

import org.exercise.ctt.adapter.input.FollowingInput;
import org.exercise.ctt.adapter.input.IInput;
import org.exercise.ctt.adapter.output.FollowingOutput;
import org.exercise.ctt.adapter.output.IOutput;
import org.exercise.ctt.exception.ConsoleTwitterException;
import org.exercise.ctt.model.User;
import org.exercise.ctt.repository.IFollowRepository;
import org.exercise.ctt.service.ValidationService;

class FollowingCommand implements ICommand{

    private IFollowRepository repository;

    public FollowingCommand(IFollowRepository repository){
        this.repository = repository;
    }

    @Override
    public IOutput execute(IInput input) throws ConsoleTwitterException {
        FollowingInput followingInput = (FollowingInput) input;
        User user = followingInput.getUser();
        User following = followingInput.getFollowing();

        validateUser(user);
        validateUser(following);
        validateSameUser(user, following);

        this.repository.addFollowing(user, following);
        return FollowingOutput.following(user, following);
    }

    private void validateSameUser(User user, User follower) throws ConsoleTwitterException {
        if(user.equals(follower)){
            throw ConsoleTwitterException.cannotFollowYourself();
        }
    }

    private void validateUser(User user) throws ConsoleTwitterException {
        ValidationService.INSTANCE.validateUser(user, this.repository);
    }
}
