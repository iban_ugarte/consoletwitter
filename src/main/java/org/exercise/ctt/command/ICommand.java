package org.exercise.ctt.command;

import org.exercise.ctt.adapter.input.IInput;
import org.exercise.ctt.adapter.output.IOutput;
import org.exercise.ctt.exception.ConsoleTwitterException;

public interface ICommand {

    IOutput execute(IInput input) throws ConsoleTwitterException;
}
