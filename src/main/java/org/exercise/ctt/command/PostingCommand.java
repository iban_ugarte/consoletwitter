package org.exercise.ctt.command;

import org.exercise.ctt.adapter.input.IInput;
import org.exercise.ctt.adapter.input.PostingInput;
import org.exercise.ctt.adapter.output.IOutput;
import org.exercise.ctt.adapter.output.PostingOutput;
import org.exercise.ctt.exception.ConsoleTwitterException;
import org.exercise.ctt.model.User;
import org.exercise.ctt.repository.IPostRepository;
import org.exercise.ctt.service.ValidationService;

class PostingCommand implements ICommand{

    IPostRepository repository;

    PostingCommand(IPostRepository repository){
        this.repository = repository;
    }

    @Override
    public IOutput execute(IInput input) throws ConsoleTwitterException {
        PostingInput postingInput = (PostingInput) input;
        validateUser(postingInput.getUser());
        this.repository.save(postingInput.getPost());
        return new PostingOutput();
    }

    private void validateUser(User user) throws ConsoleTwitterException {
        ValidationService.INSTANCE.validateUser(user, this.repository);
    }
}
