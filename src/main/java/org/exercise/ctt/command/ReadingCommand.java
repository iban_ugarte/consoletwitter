package org.exercise.ctt.command;

import org.exercise.ctt.adapter.input.IInput;
import org.exercise.ctt.adapter.input.ReadingInput;
import org.exercise.ctt.adapter.output.IOutput;
import org.exercise.ctt.adapter.output.ReadingOutput;
import org.exercise.ctt.exception.ConsoleTwitterException;
import org.exercise.ctt.model.Post;
import org.exercise.ctt.model.User;
import org.exercise.ctt.repository.IPostRepository;
import org.exercise.ctt.service.ValidationService;

import java.util.List;

class ReadingCommand implements ICommand{

    private final IPostRepository repository;

    public ReadingCommand(IPostRepository repository){
        this.repository = repository;
    }
    @Override
    public IOutput execute(IInput input) throws ConsoleTwitterException {
        ReadingInput readingInput = (ReadingInput) input;
        validateUser(readingInput.getUser());
        List<Post> postList = this.repository.findPostsByUser(readingInput.getUser());
        return ReadingOutput.of(postList);
    }

    private void validateUser(User user) throws ConsoleTwitterException {
        ValidationService.INSTANCE.validateUser(user, this.repository);
    }
}
