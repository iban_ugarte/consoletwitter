package org.exercise.ctt.command;

import org.exercise.ctt.adapter.input.IInput;
import org.exercise.ctt.adapter.input.WallInput;
import org.exercise.ctt.adapter.output.IOutput;
import org.exercise.ctt.adapter.output.WallOutput;
import org.exercise.ctt.exception.ConsoleTwitterException;
import org.exercise.ctt.model.Post;
import org.exercise.ctt.model.User;
import org.exercise.ctt.repository.IPostRepository;
import org.exercise.ctt.service.ValidationService;

import java.util.ArrayList;
import java.util.List;

class WallCommand implements ICommand{

    private IPostRepository repository;

    public WallCommand(IPostRepository repository) {
        this.repository = repository;
    }

    @Override
    public IOutput execute(IInput input) throws ConsoleTwitterException {
        WallInput wallInput = (WallInput) input;
        User user = wallInput.getUser();
        validateUser(user);

        List<Post> posts = this.repository.findPostsByUser(user);
        posts.addAll(getPostsOfUsersIamFollowing(user));

        return new WallOutput(user, posts);
    }

    private List<Post> getPostsOfUsersIamFollowing(User user){
        List<User> followingUsers = this.repository.getFollowingUsers(user);
        List<Post> posts = new ArrayList<>();
        for(User followingUser : followingUsers){
            posts.addAll(this.repository.findPostsByUser(followingUser));
        }
        return posts;
    }

    private void validateUser(User user) throws ConsoleTwitterException {
        ValidationService.INSTANCE.validateUser(user, this.repository);
    }

}
