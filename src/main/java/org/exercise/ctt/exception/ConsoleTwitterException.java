package org.exercise.ctt.exception;

import org.exercise.ctt.model.User;

public class ConsoleTwitterException extends Exception{

    final String errorMessage;

    private ConsoleTwitterException(String errorMessage){
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    private ConsoleTwitterException(String errorMessage, Throwable e){
        super(errorMessage, e);
        this.errorMessage = errorMessage;
    }

    public static ConsoleTwitterException invalidCommand(){
        return new ConsoleTwitterException("Invalid command !");
    }

    public static ConsoleTwitterException requiredMessage(){
        return new ConsoleTwitterException("Required message for posting !");
    }
    public static ConsoleTwitterException requiredUserToFollow(){
        return new ConsoleTwitterException("Required username to follow !");
    }

    public static ConsoleTwitterException invalidUser(User user){
        String error = String.format("Invalid user: %s", user.getName());
        return new ConsoleTwitterException(error);
    }

    public static ConsoleTwitterException cannotFollowYourself(){
        return new ConsoleTwitterException("You cannot follow yourself !");
    }

    public static ConsoleTwitterException unexpected(Throwable e){
        return new ConsoleTwitterException("Unexpected exception: " + e.getMessage(), e);
    }


    public String getErrorMessage() {
        return errorMessage;
    }

}
