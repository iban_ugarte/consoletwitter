package org.exercise.ctt.model;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Objects;

public class Post {
    private final User user;
    private final String message;
    private final LocalDateTime dateTime;

    private Post(User user, String message){
        this.user = user;
        this.dateTime = LocalDateTime.now();
        this.message = message;
    }

    public static Post of(User user, String message){
        return new Post(user, message);
    }

    public User getUser() {
        return user;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public String print(){
        Duration duration = Duration.between(this.dateTime, LocalDateTime.now());
        return String.format("%s (%s minutes ago)", this.message, duration.toMinutes());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o == null || getClass() != o.getClass()){
            return false;
        }
        Post post = (Post) o;
        return user.equals(post.user) && message.equals(post.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, message);
    }
}
