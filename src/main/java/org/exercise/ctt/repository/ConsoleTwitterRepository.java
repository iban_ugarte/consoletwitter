package org.exercise.ctt.repository;

public interface ConsoleTwitterRepository
        extends IPostRepository, IUserRepository, IFollowRepository {
}
