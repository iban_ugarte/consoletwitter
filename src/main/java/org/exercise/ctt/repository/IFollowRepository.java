package org.exercise.ctt.repository;

import org.exercise.ctt.model.User;

import java.util.List;

public interface IFollowRepository extends IUserRepository{
    void addFollowing(User user, User following);
    List<User> getFollowingUsers(User user);
}
