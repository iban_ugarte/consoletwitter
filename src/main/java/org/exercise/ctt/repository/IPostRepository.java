package org.exercise.ctt.repository;

import org.exercise.ctt.model.Post;
import org.exercise.ctt.model.User;

import java.util.List;

public interface IPostRepository extends IFollowRepository, IUserRepository{
    void save(Post post);
    List<Post> findPostsByUser(User user);
}
