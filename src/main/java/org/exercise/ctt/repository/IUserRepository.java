package org.exercise.ctt.repository;

import org.exercise.ctt.model.User;

import java.util.Optional;

public interface IUserRepository {
    Optional<User> findUser(User user);
}
