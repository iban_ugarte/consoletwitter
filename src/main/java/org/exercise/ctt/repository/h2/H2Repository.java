package org.exercise.ctt.repository.h2;

import org.exercise.ctt.model.Post;
import org.exercise.ctt.model.User;
import org.exercise.ctt.repository.ConsoleTwitterRepository;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class H2Repository implements ConsoleTwitterRepository {
    @Override
    public List<Post> findPostsByUser(User user) {
        return Collections.emptyList();
    }

    @Override
    public void save(Post post) {
        System.out.println("Guardamos POST en base de datos H2");
    }

    @Override
    public void addFollowing(User user, User following) {
        System.out.println("Guardamos FOLLOWER en base de datos H2");
    }

    @Override
    public List<User> getFollowingUsers(User user) {
        return Arrays.asList(new User("h2 user"));
    }

    @Override
    public Optional<User> findUser(User user) {
        System.out.println("Buscamos Usuario en base de datos H2");
        return Optional.empty();
    }
}
