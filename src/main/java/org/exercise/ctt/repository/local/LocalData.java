package org.exercise.ctt.repository.local;

import org.exercise.ctt.model.Post;
import org.exercise.ctt.model.User;

import java.util.*;

public class LocalData {

    public static List<User> USERS =
            Arrays.asList(
                    new User("bob"),
                    new User("alice"),
                    new User("charlie"),
                    new User("john"),
                    new User("sarah"));

    public static List<Post> USER_POSTS = new ArrayList<>();

    public static Map<User, List<User>> USER_FOLLOWING = new HashMap();

    private LocalData() {
    }

}
