package org.exercise.ctt.repository.local;

import org.exercise.ctt.model.Post;
import org.exercise.ctt.model.User;
import org.exercise.ctt.repository.ConsoleTwitterRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class LocalRepository implements ConsoleTwitterRepository {


    @Override
    public void save(Post post){
        LocalData.USER_POSTS.add(post);

    }

    @Override
    public List<Post> findPostsByUser(User user){
        return LocalData.USER_POSTS.stream()
                .filter(p -> p.getUser().equals(user))
                .collect(Collectors.toList());

    }

    @Override
    public void addFollowing(User user, User following){
        List<User> followingUsers = getFollowingUsers(user);
        followingUsers.add(following);
        LocalData.USER_FOLLOWING.put(user, followingUsers);
    }

    @Override
    public List<User> getFollowingUsers(User user) {
        List<User> userFollowers = LocalData.USER_FOLLOWING.get(user);
        if(Objects.isNull(userFollowers)){
            return new ArrayList<>();
        }
        return userFollowers;
    }

    @Override
    public Optional<User> findUser(User user) {
        return LocalData.USERS.stream()
                .filter(u -> u.equals(user))
                .findFirst();
    }
}
