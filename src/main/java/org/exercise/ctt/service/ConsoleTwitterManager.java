package org.exercise.ctt.service;

import org.exercise.ctt.adapter.input.IInput;
import org.exercise.ctt.adapter.input.InputHandler;
import org.exercise.ctt.adapter.output.IOutput;
import org.exercise.ctt.command.CommandFactory;
import org.exercise.ctt.command.ConsoleCommand;
import org.exercise.ctt.command.ICommand;
import org.exercise.ctt.exception.ConsoleTwitterException;
import org.exercise.ctt.repository.ConsoleTwitterRepository;

import java.util.Scanner;

public class ConsoleTwitterManager {
    
    private static final StringBuilder menu =
            new StringBuilder("Console Twitter: bob, alice, charlie, john & sarah ")
                    .append("\n================================")
                    .append("\n - Post:   [username] -> message")
                    .append("\n - Read:   [username]")
                    .append("\n - Follow: [username] follows [username]")
                    .append("\n - Wall:   [username] wall");

    private final ConsoleTwitterRepository repository;
    private final CommandFactory commandFactory;

    private ConsoleTwitterManager(ConsoleTwitterRepository repository){
        this.repository = repository;
        this.commandFactory = CommandFactory.withRepo(repository);
    }

    public static ConsoleTwitterManager withRepo(ConsoleTwitterRepository repository){
        return new ConsoleTwitterManager(repository);
    }

    public void run(){
        Scanner userInput = new Scanner(System.in);
        System.out.println(menu.toString());
        do {

            System.out.println("\nType command... ");
            String input = userInput.nextLine();
            if (!input.isEmpty()) {
                handleTypedCommand(input);

            }
        } while (true);
    }


    private void handleTypedCommand(String typedCommand){
        try {

            ConsoleCommand consoleCommand = getConsoleCommand(typedCommand);
            ICommand command = commandFactory.commandOf(getConsoleCommand(typedCommand));
            IInput input = getInput(consoleCommand, typedCommand);
            IOutput output = command.execute(input);
            output.out();

        }catch(ConsoleTwitterException e){
            System.out.println(e.getErrorMessage());
        } catch (Exception e) {
            System.out.println("Error inesperado: " + e.getMessage());
            System.exit(1);
        }
    }

    private ConsoleCommand getConsoleCommand(String input) throws Exception {
        return InputHandler.INSTANCE.getConsoleCommand(input);
    }

    private IInput getInput(ConsoleCommand consoleCommand, String consoleInput) throws Exception {
        return InputHandler.INSTANCE.getInput(consoleCommand, consoleInput);
    }
}
