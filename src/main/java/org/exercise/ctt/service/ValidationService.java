package org.exercise.ctt.service;

import org.exercise.ctt.exception.ConsoleTwitterException;
import org.exercise.ctt.model.User;
import org.exercise.ctt.repository.IUserRepository;

public enum ValidationService {

    INSTANCE;

    public void validateUser(User user, IUserRepository repository) throws ConsoleTwitterException {
        repository.findUser(user)
                .orElseThrow(()-> ConsoleTwitterException.invalidUser(user));
    }
}
