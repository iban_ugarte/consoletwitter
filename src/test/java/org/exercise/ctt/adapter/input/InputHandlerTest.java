package org.exercise.ctt.adapter.input;

import org.exercise.ctt.command.ConsoleCommand;
import org.exercise.ctt.exception.ConsoleTwitterException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class InputHandlerTest {

    @Test
    void when_TypedCommand_POST_Then_POST() throws ConsoleTwitterException {
        String typedCommand = "Bob -> post";
        ConsoleCommand consoleCommand = InputHandler.INSTANCE.getConsoleCommand(typedCommand);
        Assertions.assertEquals(ConsoleCommand.POST, consoleCommand);
    }

    @Test
    void when_TypedCommand_READ_Then_READ() throws ConsoleTwitterException {
        String typedCommand = "Bob";
        ConsoleCommand consoleCommand = InputHandler.INSTANCE.getConsoleCommand(typedCommand);
        Assertions.assertEquals(ConsoleCommand.READ, consoleCommand);
    }

    @Test
    void when_TypedCommand_NO_EXIST_Then_ConsoleTwitterException() {
        String typedCommand = "Bob >> post";
        Assertions.assertThrows(ConsoleTwitterException.class,
                ()-> InputHandler.INSTANCE.getConsoleCommand(typedCommand));
    }

    @Test
    void when_POST_Then_InputPostingInstance() throws ConsoleTwitterException {
        String typedCommand = "Bob -> post";
        IInput input = InputHandler.INSTANCE.getInput(ConsoleCommand.POST, typedCommand);
        Assertions.assertTrue(input instanceof PostingInput);
    }

    @Test
    void when_POST_withoutMessage_Then_ConsoleTwitterException() {
        String typedCommand = "Bob ->";
        Assertions.assertThrows(ConsoleTwitterException.class,
                ()-> InputHandler.INSTANCE.getInput(ConsoleCommand.POST, typedCommand));

    }

    @Test
    void when_FOLLOW_Then_FollowingInputInstance() throws ConsoleTwitterException {
        String typedCommand = "Bob follows whoever";
        IInput input = InputHandler.INSTANCE.getInput(ConsoleCommand.FOLLOW, typedCommand);
        Assertions.assertTrue(input instanceof FollowingInput);
    }

    @Test
    void when_FOLLOW_withoutFollowing_Then_ConsoleTwitterException() {
        String typedCommand = "Bob follows ";
        Assertions.assertThrows(ConsoleTwitterException.class,
                ()-> InputHandler.INSTANCE.getInput(ConsoleCommand.FOLLOW, typedCommand));

    }

    @Test
    void when_READ_Then_ReadingInputInstance() throws ConsoleTwitterException {
        String typedCommand = "Bob";
        IInput input = InputHandler.INSTANCE.getInput(ConsoleCommand.READ, typedCommand);
        Assertions.assertTrue(input instanceof ReadingInput);
    }

    @Test
    void when_WALL_Then_WallInputInstance() throws ConsoleTwitterException {
        String typedCommand = "Bob wall";
        IInput input = InputHandler.INSTANCE.getInput(ConsoleCommand.WALL, typedCommand);
        Assertions.assertTrue(input instanceof WallInput);
    }


}