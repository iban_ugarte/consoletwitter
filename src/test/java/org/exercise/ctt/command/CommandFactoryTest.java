package org.exercise.ctt.command;

import org.exercise.ctt.repository.ConsoleTwitterRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class CommandFactoryTest {

    CommandFactory commandFactory;

    @BeforeEach
    void setUp(){
        this.commandFactory = CommandFactory.withRepo(Mockito.mock(ConsoleTwitterRepository.class));
    }

    @Test
    void when_POST_command_Then_PostingCommand() {
        ICommand command = this.commandFactory.commandOf(ConsoleCommand.POST);
        Assertions.assertTrue(command instanceof PostingCommand);
    }

    @Test
    void when_FOLLOW_command_Then_FollowingCommand() {
        ICommand command = this.commandFactory.commandOf(ConsoleCommand.FOLLOW);
        Assertions.assertTrue(command instanceof FollowingCommand);
    }

    @Test
    void when_READ_command_Then_ReadingCommand() {
        ICommand command = this.commandFactory.commandOf(ConsoleCommand.READ);
        Assertions.assertTrue(command instanceof ReadingCommand);
    }

    @Test
    void when_WALL_command_Then_WallCommand() {
        ICommand command = this.commandFactory.commandOf(ConsoleCommand.WALL);
        Assertions.assertTrue(command instanceof WallCommand);
    }

}