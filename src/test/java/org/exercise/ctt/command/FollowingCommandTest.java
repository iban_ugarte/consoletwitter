package org.exercise.ctt.command;

import org.exercise.ctt.adapter.input.FollowingInput;
import org.exercise.ctt.adapter.input.IInput;
import org.exercise.ctt.adapter.output.FollowingOutput;
import org.exercise.ctt.adapter.output.IOutput;
import org.exercise.ctt.exception.ConsoleTwitterException;
import org.exercise.ctt.model.User;
import org.exercise.ctt.repository.ConsoleTwitterRepository;
import org.exercise.ctt.repository.local.LocalRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;

class FollowingCommandTest {


    FollowingCommand command;
    ConsoleTwitterRepository repository = Mockito.mock(LocalRepository.class);

    User user = new User("user");
    User following = new User("following");

    @BeforeEach
    void setUp() {
        this.command = new FollowingCommand(repository);
        Mockito.doNothing().when(this.repository).addFollowing(user, following);
    }

    @Test
    void when_OK_Then_FollowingOutput_Not_Null() throws ConsoleTwitterException {
        Mockito.when(this.repository.findUser(user)).thenReturn(Optional.of(user));
        Mockito.when(this.repository.findUser(following)).thenReturn(Optional.of(following));
        IOutput output = this.command.execute(getFollowingInput());
        Assertions.assertNotNull(output);
    }

    @Test
    void when_UserNotFound_Then_InvalidUser() {
        Mockito.when(this.repository.findUser(user)).thenReturn(Optional.empty());
        assertThrows(ConsoleTwitterException.class,
                () -> this.command.execute(getFollowingInput()));
    }

    @Test
    void when_FollowingUserNotFound_Then_InvalidUser() {
        Mockito.when(this.repository.findUser(user)).thenReturn(Optional.of(user));
        Mockito.when(this.repository.findUser(following)).thenReturn(Optional.empty());
        assertThrows(ConsoleTwitterException.class,
                () -> this.command.execute(getFollowingInput()));
    }

    @Test
    void when_SameUserThen_CannotFollowYourself() {
        FollowingInput input = new FollowingInput(user, user);
        Mockito.when(this.repository.findUser(user)).thenReturn(Optional.of(user));
        assertThrows(ConsoleTwitterException.class,
                () -> this.command.execute(input));
    }

    private IInput getFollowingInput(){
        return new FollowingInput(user, following);
    }

    private IOutput getFollowingOutput(){
        return FollowingOutput.following(user, following);
    }

}