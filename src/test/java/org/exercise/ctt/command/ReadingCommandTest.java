package org.exercise.ctt.command;

import org.exercise.ctt.adapter.input.IInput;
import org.exercise.ctt.adapter.input.ReadingInput;
import org.exercise.ctt.adapter.output.IOutput;
import org.exercise.ctt.adapter.output.ReadingOutput;
import org.exercise.ctt.exception.ConsoleTwitterException;
import org.exercise.ctt.model.Post;
import org.exercise.ctt.model.User;
import org.exercise.ctt.repository.ConsoleTwitterRepository;
import org.exercise.ctt.repository.local.LocalRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ReadingCommandTest {

    ReadingCommand command;
    ConsoleTwitterRepository repository = Mockito.mock(LocalRepository.class);

    User user = new User("user");
    Post post = Post.of(user, "message");
    List<Post> postList = Collections.singletonList(post);

    @BeforeEach
    void setUp() {
        this.command = new ReadingCommand(repository);
        Mockito.when(this.repository.findPostsByUser(user)).thenReturn(postList);
    }

    @Test
    void when_OK_Then_Output_Not_Null() throws ConsoleTwitterException {
        Mockito.when(this.repository.findUser(user)).thenReturn(Optional.of(user));
        IOutput output = this.command.execute(getInput());
        Assertions.assertNotNull(output);
    }

    @Test
    void when_UserNotFound_Then_InvalidUser() {
        Mockito.when(this.repository.findUser(user)).thenReturn(Optional.empty());
        assertThrows(ConsoleTwitterException.class,
                () -> this.command.execute(getInput()));
    }

    private IInput getInput(){
        return new ReadingInput(user);
    }

    private IOutput getOutput(){
        return ReadingOutput.of(postList);
    }
}