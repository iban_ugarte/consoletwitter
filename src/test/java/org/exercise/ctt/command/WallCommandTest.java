package org.exercise.ctt.command;

import org.exercise.ctt.adapter.input.IInput;
import org.exercise.ctt.adapter.input.WallInput;
import org.exercise.ctt.adapter.output.IOutput;
import org.exercise.ctt.adapter.output.WallOutput;
import org.exercise.ctt.exception.ConsoleTwitterException;
import org.exercise.ctt.model.Post;
import org.exercise.ctt.model.User;
import org.exercise.ctt.repository.ConsoleTwitterRepository;
import org.exercise.ctt.repository.local.LocalRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertThrows;

class WallCommandTest {
    WallCommand command;
    ConsoleTwitterRepository repository = Mockito.mock(LocalRepository.class);

    User user = new User("user");
    Post post = Post.of(user, "message");
    List<Post> postList = new ArrayList<>(Collections.singletonList(post));
    List<User> followingUsers = Collections.singletonList(user);

    @BeforeEach
    void setUp() {
        this.command = new WallCommand(repository);
        Mockito.when(this.repository.findPostsByUser(user)).thenReturn(postList);
        Mockito.when(this.repository.getFollowingUsers(user)).thenReturn(followingUsers);
    }

    @Test
    void when_OK_Then_Output_Not_Null() throws ConsoleTwitterException {
        Mockito.when(this.repository.findUser(user)).thenReturn(Optional.of(user));
        IOutput output = this.command.execute(getInput());
        Assertions.assertNotNull(output);
    }

    @Test
    void when_UserNotFound_Then_InvalidUser() {
        Mockito.when(this.repository.findUser(user)).thenReturn(Optional.empty());
        assertThrows(ConsoleTwitterException.class,
                () -> this.command.execute(getInput()));
    }

    private IInput getInput(){
        return new WallInput(user);
    }

    private IOutput getOutput(){
        return new WallOutput(user, postList);
    }
}