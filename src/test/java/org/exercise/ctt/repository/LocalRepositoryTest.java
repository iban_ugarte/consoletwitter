package org.exercise.ctt.repository;

import org.exercise.ctt.model.Post;
import org.exercise.ctt.model.User;
import org.exercise.ctt.repository.local.LocalRepository;
import org.junit.jupiter.api.*;

import java.util.List;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class LocalRepositoryTest {

    LocalRepository localRepository = new LocalRepository();

    @Test
    @Order(1)
    void whenSavePost_ThenTrue(){
        User user = new User("Bob");
        Post post = Post.of(user,"test post");
        localRepository.save(post);
        Assertions.assertTrue(true);
    }

    @Test
    @Order(2)
    void whenFindByUserBob_ThenOnePost() {
        User user = new User("Bob");
        List<Post> posts = localRepository.findPostsByUser(user);
        Assertions.assertEquals(1, posts.size());

    }

    @Test
    @Order(3)
    void whenFindByUserFoo_ThenNoPost() {
        User user = new User("Foo");
        List<Post> posts = localRepository.findPostsByUser(user);
        Assertions.assertTrue(posts.isEmpty());

    }



}