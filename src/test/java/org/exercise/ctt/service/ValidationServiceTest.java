package org.exercise.ctt.service;

import org.exercise.ctt.exception.ConsoleTwitterException;
import org.exercise.ctt.model.User;
import org.exercise.ctt.repository.ConsoleTwitterRepository;
import org.exercise.ctt.repository.local.LocalRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ValidationServiceTest {

    ConsoleTwitterRepository repository = Mockito.mock(LocalRepository.class);

    User user = new User("user");

    @BeforeEach
    void setUp() {
    }

    @Test
    void whenValidateUser_OK() {
        Mockito.when(this.repository.findUser(user)).thenReturn(Optional.of(user));
        assertDoesNotThrow(()-> ValidationService.INSTANCE.validateUser(user, this.repository));
    }

    @Test
    void whenValidateUser_invalidUser() {
        Mockito.when(this.repository.findUser(user)).thenReturn(Optional.empty());
        assertThrows(ConsoleTwitterException.class,
                ()-> ValidationService.INSTANCE.validateUser(user, this.repository));
    }
}